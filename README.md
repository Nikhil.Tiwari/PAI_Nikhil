How to run the program?
<br>
One way is to use 
<br>
`python3 train.py all <model_name> <epochs> <lr>`
<br>
model_name can be 'inception' or 'cnn'
<br><br>
Possible error "from paths import ..." paths is not recognised => change to "from .paths import ...." it works
<br><br>
Best way to check each functionality is to use terminal and run separate function as follows:
<br><br>

Please go through data.py which is used to prepare data, the file is well commented to understand and pre-process data
<br>
Import functions in terminal and run them to generate results
<br><br>
<b>
Crete a directory named data/data and add the training data or change directory path in src/paths.py<br>
Create a directory named "model_input_data" in /inferences and
Please uncomment create_and_save_model_input() in data.py and comment import of .npy files in train.py, it can be used for subsequent fast training</b>

<br><br><b>The whole working code could not be pushed on gitlab due to memory limits. I got error "disk out of space" that's why data needs to be added before running code it would be better to run functions separately from terminal starting with <br>
prepare_data() from data.py
Then run training.py
</b>

